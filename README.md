# Periodical Numbers

- [Description](#description)
  - [Input data and Objective](#input-data-and-objective)
  - [Function *find_period*](#function-find_period)
  - [Algorithm](#algorithm)
- [Testing result](#testing-result)

## Description

Link to problem on Timus: https://acm.timus.ru/problem.aspx?space=1&num=1558

The software is designed to determine the repeating and non-repeating part from the sum of two given inverted periodic numbers. The solution is based on the realization of the function of searching for a substring in a periodic sequence of numbers.

### Input data and Objective

Two infinite sequences of numbers in brackets are given as input. These sequences are summed up, and one sequence is obtained. 

The task is to determine the period of this sequence, i.e. to find a combination of numbers that will be a substring for the resulting infinite string.

### Function *find_period*

The input of the function is a sequence-string in which its substrings will be searched. The point of this function is to return a certain slice of the sequence-string, where the desired combination of digits will lie. To understand which slice to return, we need to find the first occurrence of the *first_occurrence* substring in the sequence string. For this purpose, the input string is doubled and the *find(sequence, 1, -1)* method is used, where sequence is the combination of digits to be searched, 1 is the index from which the search is performed, -1 means that the search is performed until the end of the doubled input string. If no matches are found, return 0.

![Function find_period](https://gitlab.com/fantic-university-projects/object-oriented-programming/periodical-numbers/-/raw/main/Resource/find_period.jpg?ref_type=heads "Function find_period visualisation")

### Algorithm

The process of summing two summands, depending on the digits, can give two different types of sequences: the size of the sequence is equal to the size of one of the summands, the size of the sequence will be 1 more than the size of the summand. The algorithm is to find this type correctly and apply the *find_period()* function to it.

First there is a check for the length of the new sequence. If it is equal to the length of one of the summands, it means that we have a simple new sequence to which we can apply the function. Let's print the result in brackets. If it is less than the length of the summand, it means that the resulting sum has naked zeros on the left and python removed them automatically. To make the function work with a non-broken sequence, return the removed number of zeros. To do this, we create a string of zeros and count their number as the difference between the length of the tripled summand and the length of the resulting sum. To the resulting string of zeros we assign the broken sequence string (the sum of numbers without zeros on the left). The result is a sequence of the correct size, namely the size of one of the summands, to which the *find_period()* function is applied, and the combination sought is printed in brackets. These were the cases when the size of the sequence is equal to the size of the summand.

If, however, when adding two numbers, the new sequence will be the size of the length of the summand + 1. This means that the sequence will consist of a periodic combination of numbers, which will be written in brackets, and a non-periodic part, written after the brackets, without spaces. So, first, a variable *part_of_sequence* is created, which contains a slice of the sequence, namely, the third part of the elements is discarded from the front and the last element is discarded from the back. This sequence must be checked by the function, because it may turn out that it contains the combination of digits you are looking for. If so, the combination is printed in brackets, followed by the discarded digit. If the function fails to find such a combination, a while loop is started with the condition that it will not end until the function finds the required digits. The previously initialized shift boundary is incremented by 1 at each loop pass. The value of *part_of_sequence* is recalculated, now taking into account the *shift*, thus shifting the boundaries of the sought sequence to the left. Now it becomes clear why the first third of the characters in *part_of_sequence* was discarded, this allows us to move through the entire resulting sum-sequence. After changing the sequence, it asks if the combination was found, if not, the loop repeats. If yes, exit the loop, output the found combination in brackets, followed by all the shifted elements.

# Testing result

![Timus testing result](https://gitlab.com/fantic-university-projects/object-oriented-programming/periodical-numbers/-/raw/main/Resource/TimusOnlineJudgeTesting.png?ref_type=heads "Timus testing result presentation")

When executed, the program uses no more than 512 KB of RAM and runs for no more than 0.093 seconds.